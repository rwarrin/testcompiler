@echo off

SET CompilerFlags=/nologo /Od /Z7 /MTd /Wall /W1 /fp:fast
SET LinkerFlags=/incremental:no

if not exist ..\build mkdir ..\build
pushd ..\build

cl.exe %CompilerFlags% ..\code\main.c /link %LinkerFlags%

popd
