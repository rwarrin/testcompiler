#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <stdarg.h>
#include <string.h>

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef float real32;
typedef double real64;
typedef int32 bool32;

#define true 1
#define false 0

#define ArrayCount(Array) (sizeof((Array)) / (sizeof((Array)[0])))

static uint8 *GlobalReservedWordList[32];
static int32 GlobalReservedWordListCount;

enum ReservedWordId
{
	ReservedWordId_Let = 0,
	ReservedWordId_Function,
	ReservedWordId_Tag,
	ReservedWordId_Scenario,
	ReservedWordId_Given,
	ReservedWordId_When,
	ReservedWordId_Then,
	ReservedWordId_Module,
	ReservedWordId_Feature,

	ReservedWordId_Count
};

#define SyntaxError(Fmt, ...) _SyntaxError(Tokenizer, Fmt, ##__VA_ARGS__)
static inline void
_SyntaxError(struct tokenizer *Tokenizer, char *Format, ...)
{
	va_list Args;
	va_start(Args, Format);
	// TODO(rick): Add filename, line number, col to Tokenizer
	printf("\n%s line %d col %d | Syntax Error: ", "somefile.taf", 10, 0);
	vprintf(Format, Args);
	printf("\n");
	va_end(Args);
}

static inline bool32
IsAlpha(uint8 Char)
{
	bool32 Result = ( ((Char >= 'a') && (Char <= 'z')) || 
					  ((Char >= 'A') && (Char <= 'Z')) );
	return(Result);
}

static inline bool32
IsNumeric(uint8 Char)
{
	bool32 Result = ( (Char >= '0') && (Char <= '9') );
	return(Result);
}

static inline bool32
IsAlphaNumeric(uint8 Char)
{
	bool32 Result = IsAlpha(Char) || IsNumeric(Char);
	return(Result);
}

static inline bool32
IsWhitespace(uint8 Char)
{
	bool32 Result = ( (Char == ' ') ||
					  (Char == '\t') ||
					  (Char == '\r') ||
					  (Char == '\n') );
	return(Result);
}

struct intern_string
{
	uint8 *String;
	int32 Length;
};

struct string_intern_table
{
	struct intern_string *Table;
	int32 TableSize;
	int32 TableCount;
};

struct string_intern_table *GlobalStringInternTable;

#define InternString(String) _InternString((String), strlen((String)))
#define InternStringFragment(String, Length) _InternString((String), ((Length)))
static uint8 *
_InternString(uint8 *String, int32 Length)
{
	uint8 *Result = 0;

	if(GlobalStringInternTable == NULL)
	{
		GlobalStringInternTable = (struct string_intern_table *)malloc(sizeof(struct string_intern_table));
		assert(GlobalStringInternTable != NULL);
		GlobalStringInternTable->TableSize = 1;
		GlobalStringInternTable->TableCount = 0;
		GlobalStringInternTable->Table = (struct intern_string *)malloc(sizeof(struct intern_string) * GlobalStringInternTable->TableSize);
		assert(GlobalStringInternTable->Table != NULL);
	}

	for(int32 TableIndex = 0; TableIndex < GlobalStringInternTable->TableCount; ++TableIndex)
	{
		struct intern_string *InternString = GlobalStringInternTable->Table + TableIndex;
		if((InternString->Length == Length) &&
		   (strncmp(InternString->String, String, Length) == 0))
		{
			Result = GlobalStringInternTable->Table[TableIndex].String;
			break;
		}
	}

	if(Result == 0)
	{
		if(GlobalStringInternTable->TableCount + 1 > GlobalStringInternTable->TableSize)
		{
			struct intern_string *NewTable = (struct intern_string *)realloc(GlobalStringInternTable->Table, sizeof(struct intern_string) * GlobalStringInternTable->TableSize * 2);
			if(NewTable != NULL)
			{
				GlobalStringInternTable->Table = NewTable;
				GlobalStringInternTable->TableSize = GlobalStringInternTable->TableSize * 2;
			}
		}

		struct intern_string *InternString = GlobalStringInternTable->Table + GlobalStringInternTable->TableCount;
		InternString->String = (uint8 *)malloc(sizeof(uint8 *) * (Length + 1));
		if(InternString->String != NULL)
		{
			memcpy(InternString->String, String, Length);
			InternString->String[Length] = 0;
			InternString->Length = Length;
			Result = InternString->String;
			++GlobalStringInternTable->TableCount;
		}
	}

	assert(Result != 0);
	return(Result);
}

struct tokenizer
{
	uint8 *At;
};

enum TokenType
{
	TokenType_Unknown = 0,

	TokenType_Identifier,
	TokenType_String,
	TokenType_Integer,
	TokenType_Float,

	TokenType_LeftParenthesis,
	TokenType_RightParenthesis,
	TokenType_LeftBracket,
	TokenType_RightBracket,
	TokenType_LeftBrace,
	TokenType_RightBrace,

	TokenType_Equal,

	TokenType_Semicolon,

	TokenType_EndOfStream,
	TokenType_Count
};

struct token
{
	enum TokenType Type;
	uint8 *Start;
	uint8 *End;
	int32 Length;
	union {
		uint8 *Name;
		real64 Float;
		int32 Integer;
	};
};

static struct tokenizer
InitTokenizer(uint8 *Stream)
{
	struct tokenizer Tokenizer = {0};
	Tokenizer.At = Stream;
	return(Tokenizer);
}

static void
ConsumeWhitespace(struct tokenizer *Tokenizer)
{
	while(IsWhitespace(*Tokenizer->At))
	{
		++Tokenizer->At;
	}
}

static struct token
GetToken(struct tokenizer *Tokenizer)
{
	ConsumeWhitespace(Tokenizer);

	struct token Token = {0};
	Token.Start = Tokenizer->At;
	Token.Length = 1;

	uint8 Character = *Tokenizer->At++;
	switch(Character)
	{
		case 0: { Token.Type = TokenType_EndOfStream; } break;
		case '(': { Token.Type = TokenType_LeftParenthesis; } break;
		case ')': { Token.Type = TokenType_RightParenthesis; } break;
		case '[': { Token.Type = TokenType_LeftBracket; } break;
		case ']': { Token.Type = TokenType_RightBracket; } break;
		case '{': { Token.Type = TokenType_LeftBrace; } break;
		case '}': { Token.Type = TokenType_RightBrace; } break;
		case '=': { Token.Type = TokenType_Equal; } break;
		case ';': { Token.Type = TokenType_Semicolon; } break;
		case '"': {
			Token.Type = TokenType_String;
			while((*Tokenizer->At != 0) && (*Tokenizer->At != '"'))
			{
				if(*Tokenizer->At == '\n')
				{
					SyntaxError("Illegal new line in string");
				}

				if((*Tokenizer->At == '\\') && (*(Tokenizer->At + 1) == '"'))
				{
					++Tokenizer->At;
				}
				++Tokenizer->At;
			}
			++Tokenizer->At;
			Token.End = Tokenizer->At;
			Token.Length = Tokenizer->At - Token.Start;
		} break;
		default: {
			if(IsAlpha(Character))
			{
				Token.Type = TokenType_Identifier;
				while( (*Tokenizer->At != 0) &&
					   ((IsAlphaNumeric(*Tokenizer->At) ||
						 *Tokenizer->At == '_')) )
				{
					++Tokenizer->At;
				}
				Token.End = Tokenizer->At;
				Token.Length = Tokenizer->At - Token.Start;
				Token.Name = InternStringFragment(Token.Start, Token.Length);
			}
			else if(IsNumeric(Character) || (Character == '-'))
			{
				Token.Type = TokenType_Integer;
				while( (*Tokenizer->At != 0) && ((IsNumeric(*Tokenizer->At) || *Tokenizer->At == '.')) )
				{
					if(*Tokenizer->At == '.')
					{
						Token.Type = TokenType_Float;
					}
					++Tokenizer->At;
				}
				Token.End = Tokenizer->At;
				Token.Length = Tokenizer->At - Token.Start;
			}
		} break;
	}

	return(Token);
}

static bool32
IsToken(struct token Token, enum TokenType Type)
{
	bool32 Result = (Token.Type == Type);
	return(Result);
}

static bool32
ExpectToken(struct tokenizer *Tokenizer, enum TokenType Type, struct token *TokenOut)
{
	struct token Token = GetToken(Tokenizer);
	bool32 Result = (Token.Type == Type);

	if(TokenOut != 0)
	{
		*TokenOut = Token;
	}

	return(Result);
}

static struct token
PeekToken(struct tokenizer *Tokenizer)
{
	uint8 *SavedLocation = Tokenizer->At;
	struct token Token = GetToken(Tokenizer);
	Tokenizer->At = SavedLocation;
	return(Token);
}

enum DeclType
{
	DeclType_Unknown = 0,
	DeclType_Variable,
	DeclType_Function,
	DeclType_Literal,
	DeclType_Count
};

struct decl_buffer
{
	enum DeclType Type;
	struct decl *Buffer;
	int32 Count;
	int32 Size;
};

struct decl_variable
{
	uint8 *Name;
	uint8 *Value;
};

struct decl_function
{
	uint8 *Name;
	uint8 *Start;
	int32 Length;
};

struct decl
{
	enum DeclType Type;
	union
	{
		struct decl_variable Variable;
		struct decl_function Function;
	};
};

static struct decl_buffer
NewDeclBuffer()
{
	struct decl_buffer Result = {0};
	Result.Count = 0;
	Result.Size = 1;
	Result.Buffer = (struct decl *)malloc(sizeof(struct decl) * Result.Size);
	return(Result);
}

static void
DeclBufferPush(struct decl_buffer *Buffer, struct decl Decl)
{
	if(Buffer->Count + 1 > Buffer->Size)
	{
		int32 NewSize = Buffer->Size * 2;
		struct decl *NewBuffer = (struct decl *)realloc(Buffer->Buffer, sizeof(struct decl) * NewSize);
		if(NewBuffer != 0)
		{
			Buffer->Buffer = NewBuffer;
			Buffer->Size = NewSize;
		}
	}

	Buffer->Buffer[Buffer->Count++] = Decl;
}

static struct decl *
DeclBufferSearchInternString(struct decl_buffer *Buffer, uint8 *Str)
{
	struct decl *Result = 0;

	for(int32 Index = 0; Index < Buffer->Count; ++Index)
	{
		struct decl *Test = (Buffer->Buffer + Index);
		if(Test->Type == DeclType_Variable)
		{
			if(Test->Variable.Name == Str)
			{
				Result = Test;
				break;
			}
		}
		else if(Test->Type == DeclType_Function)
		{
			if(Test->Function.Name == Str)
			{
				Result = Test;
				break;
			}
		}
	}

	return(Result);
}

static struct decl
ParseVariableDecl(struct tokenizer *Tokenizer)
{
	struct decl_variable ResultData = {0};
	struct decl Result = {0};

	struct token TempToken = {0};
	if(ExpectToken(Tokenizer, TokenType_Identifier, &TempToken))
	{
		ResultData.Name = TempToken.Name;
	}
	else
	{
		SyntaxError("expected identifier got '%.*s'", TempToken.Length, TempToken.Start);
	}

	if(!ExpectToken(Tokenizer, TokenType_Equal, &TempToken))
	{
		SyntaxError("expected '=' got '%.*s'", TempToken.Length, TempToken.Start);
	}

	TempToken = GetToken(Tokenizer);
	if(IsToken(TempToken, TokenType_Identifier) ||
	   IsToken(TempToken, TokenType_String) ||
	   IsToken(TempToken, TokenType_Integer) ||
	   IsToken(TempToken, TokenType_Float))
	{
		ResultData.Value = InternStringFragment(TempToken.Start, TempToken.Length);
	}
	else
	{
		SyntaxError("Expected an Identifer, String, Integer, or Float");
	}

	Result.Type = DeclType_Variable;
	Result.Variable = ResultData;
	return(Result);
}

static uint8 *
ConsumeFunction(struct tokenizer *Tokenizer)
{
	uint8 *Result = 0;

	while(*Tokenizer->At != '{')
	{
		++Tokenizer->At;
	}

	++Tokenizer->At;
	int32 ScopeCounter = 1;
	for(;;)
	{
		if(*Tokenizer->At == 0)
		{
			SyntaxError("End of stream encountered inside function scope");
		}

		if(*Tokenizer->At == '{')
		{
			++ScopeCounter;
		}
		else if(*Tokenizer->At == '}')
		{
			--ScopeCounter;
		}

		if(ScopeCounter == 0)
		{
			Result = Tokenizer->At;
			break;
		}

		++Tokenizer->At;
	}

	return(Result);
}

static struct decl
ParseFunctionDecl(struct tokenizer *Tokenizer)
{
	struct decl_function ResultData = {0};
	struct decl Result = {0};

	struct token Token = {0};
	if(!ExpectToken(Tokenizer, TokenType_Identifier, &Token))
	{
		SyntaxError("Expected identifer got '%.*s'", Token.Length, Token.Start);
	}
	else
	{
		ResultData.Name = Token.Name;
	}

	ResultData.Start = Token.Start;
	uint8 *End = ConsumeFunction(Tokenizer);
	ResultData.Length = End - Token.Start;

	if(!ExpectToken(Tokenizer, TokenType_RightBrace, &Token))
	{
		SyntaxError("Expected '}' got '%.*s'", Token.Length, Token.Start);
	}

	Result.Type = DeclType_Function;
	Result.Function = ResultData;

	return(Result);
}

static inline uint8 *
AddReservedWord(uint8 *String)
{
	assert((GlobalReservedWordListCount + 1) <= ArrayCount(GlobalReservedWordList));

	uint8 *InternedWord = InternString(String);
	GlobalReservedWordList[GlobalReservedWordListCount++] = InternedWord;

	return(InternedWord);
}

static bool32
IsReservedWord(struct token Token)
{
	bool32 Result = false;

	for(int32 Index = 0; Index < ArrayCount(GlobalReservedWordList); ++Index)
	{
		if((GlobalReservedWordList[Index] != 0) &&
		   (Token.Name == GlobalReservedWordList[Index]))
		{
			Result = true;
			break;
		}
	}

	return(Result);
}

static struct decl_buffer GlobalVariableBuffer;
static struct decl_buffer GlobalFunctionBuffer;

static void
ParseStream(struct tokenizer Tokenizer)
{
	struct token Token = {0};
	while((Token = GetToken(&Tokenizer)).Type != TokenType_EndOfStream)
	{
		printf("Token: ");
		switch(Token.Type)
		{
			case TokenType_Unknown: {
				printf("Unknown token");
			} break;
			case TokenType_Identifier: {
				if(IsReservedWord(Token))
				{
					printf("Reserved word identifier: %s", Token.Name);
					struct decl Decl = {0};
					if(Token.Name == InternString("let"))
					{
						Decl = ParseVariableDecl(&Tokenizer);
						DeclBufferPush(&GlobalVariableBuffer, Decl);
					}
					else if(Token.Name == InternString("function"))
					{
						Decl = ParseFunctionDecl(&Tokenizer);
					}
					else
					{
						printf("Unhandled reserved word %s\n", Token.Name);
					}
				}
				else
				{
					printf("Identifier %.*s", Token.Length, Token.Start);
				}
			} break;
			case TokenType_String: {
				printf("String: %.*s", Token.Length, Token.Start);
			} break;
			case TokenType_Float:
			case TokenType_Integer: {
				printf("Number: %.*s", Token.Length, Token.Start);
			} break;
			case TokenType_LeftParenthesis: {
				printf("Left Parenthesis");
			} break;
			case TokenType_RightParenthesis: {
				printf("Right Parenthesis");
			} break;
			case TokenType_LeftBracket: {
				printf("Left Bracket");
			} break;
			case TokenType_RightBracket: {
				printf("Right Bracket");
			} break;
			case TokenType_LeftBrace: {
				printf("Left Brace");
			} break;
			case TokenType_RightBrace: {
				printf("Right Brace");
			} break;
			case TokenType_Equal: {
				printf("Equal");
			} break;
			case TokenType_Semicolon: {
				printf("Semicolon");
			} break;
			case TokenType_EndOfStream: {
				printf("End Of Stream");
			} break;
			case TokenType_Count: {
				assert(!"Can't have token type Count");
			} break;
			default: {
				printf("Unhandled token type");
			} break;
		}
		printf("\n");
	}
}

/**
 * I need to define a grammar
 *
 * TODO(rick): define a grammar...?
 * Statement: Literal | 'let' Name '=' Literal | ReservedWord Literal | '{code}' ... '{code}'
 * Name: [a-Z A-Z][0-9]_
 * Literal: [-0-9.] | Name | Name '(' Literal ')'
 *
 * type_list = type (',' type)
 *
 * base_type = NAME
 *			 | 'func' '(' type_list? ')'
 *			 | '(' type ')'
 *
 * type = base_type
 *
 * let_decl = NAME '=' expr
 *
 * func_param = NAME
 * func_param_list = func_param (',' func_param)
 * func_decl = NAME '(' func_param_list? ')' statement_block
 * I don't really care about your func just that you have one.. I'm just going
 * to copy it and transform it...
 *
 * decl = 'let' let_decl
 *		| 'function' func_decl
 *
 * Statements:
 * statement = 'return' expr ';'
 *			 | expr 
 **/

int
main(void)
{
	AddReservedWord("let");
	AddReservedWord("function");
	AddReservedWord("@");
	AddReservedWord("Scenario");
	AddReservedWord("Given");
	AddReservedWord("When");
	AddReservedWord("Then");
	AddReservedWord("Module");
	AddReservedWord("Feature");

	GlobalVariableBuffer = NewDeclBuffer();
	GlobalFunctionBuffer = NewDeclBuffer();

	struct tokenizer Tokenizer = InitTokenizer("Given \"A user is on some page\" and there are 5 things");
	//ParseStream(Tokenizer);

	Tokenizer = InitTokenizer("When \"A user clicks on some link\"");
	//ParseStream(Tokenizer);

	Tokenizer = InitTokenizer("Then \"Verifiy that the page title is something\"");
	//ParseStream(Tokenizer);

	Tokenizer = InitTokenizer("let MyVariable = \"body > div.somestuff > h1\";\nlet MyOtherVariable = \"Title\";");
	ParseStream(Tokenizer);

	Tokenizer = InitTokenizer("CallSomeFunction({{MyVariable}}, {{MyOtherVariable}});");
	//ParseStream(Tokenizer);

	Tokenizer = InitTokenizer("function SomeFunction(a, b) {\n\tif(a > b)\n{\nreturn a + b}\nelse\n{\nreturn a - b;\n}\n}");
	ParseStream(Tokenizer);

	return 0;
}
